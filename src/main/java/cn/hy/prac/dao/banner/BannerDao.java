package cn.hy.prac.dao.banner;

import java.util.List;

import org.apache.ibatis.annotations.Select;

import cn.hy.prac.entities.banner.Banner;

public interface BannerDao {

	@Select("select * from user_info order by id desc")
	public List<Banner> getBannerList();
	
	
}
