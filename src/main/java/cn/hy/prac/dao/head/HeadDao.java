package cn.hy.prac.dao.head;

import java.util.List;

import org.apache.ibatis.annotations.Select;

import cn.hy.prac.entities.head.Head;

public interface HeadDao {

	@Select("SELECT * FROM bulutest")
	public List<Head> getHeadList();
}
