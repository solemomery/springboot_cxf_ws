package cn.hy.prac.constant;

public class Constant {

	//session中存储当前登录的用户key
	public static final String SESSION_USER_KEY = "banner";

	//当前页面最多展现多少条数据
	public static final int PAGE_MAX = 10;
}
