package cn.hy.prac.activemq;

import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;

/**
 * MQ消费者
 * @author Administrator
 *
 */
@Component
public class ActiveMqConsumer {

	   /**使用@JmsListener注解来监听指定的某个队列内的消息,是否有新增,有的话则取出队列内消息
	   *进行处理
	   **/
	  // @JmsListener(destination="bannerListSize")
	   public void removeMessage(String msg){
	  
		   /**
		    * 处理MQ消息
		    */
		   
		   System.out.println("监听接收到的消息是-------"+msg);//打印队列内的消息
	   }

}
