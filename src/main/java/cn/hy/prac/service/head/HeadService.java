package cn.hy.prac.service.head;

import java.util.List;

import javax.jws.WebService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsMessagingTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import cn.hy.prac.dao.head.HeadDao;
import cn.hy.prac.entities.head.Head;

@WebService
@Service
@Transactional
public class HeadService {

	@Autowired
	private HeadDao headDao;
	
	/**
	 * 
	 * @return
	 */
	public List<Head> getHeadList(){
		return headDao.getHeadList();
	}
}
