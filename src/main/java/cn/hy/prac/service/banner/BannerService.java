package cn.hy.prac.service.banner;

import java.util.List;

import javax.annotation.Resource;
import javax.jws.WebService;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import cn.hy.prac.dao.banner.BannerDao;
import cn.hy.prac.entities.banner.Banner;

@Transactional
@Service("BannerService")
@WebService
public class BannerService {

	@Resource
	private BannerDao bannerDao;
	
	public List<Banner> getBannerList(){
		return bannerDao.getBannerList();
	}
}
