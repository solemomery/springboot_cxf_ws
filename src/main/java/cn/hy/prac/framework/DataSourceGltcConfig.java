package cn.hy.prac.framework;

import javax.sql.DataSource;

import org.springframework.boot.autoconfigure.jdbc.DataSourceBuilder;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class DataSourceGltcConfig {

	@Bean(name = "dsgltc")
	@ConfigurationProperties(prefix = "spring.dsgltc")
	public DataSource gltcDataSource() {
		return DataSourceBuilder.create().build();
	}

}
