package cn.hy.prac.framework;

import javax.xml.ws.Endpoint;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import cn.hy.prac.service.banner.BannerService;
import cn.hy.prac.service.head.HeadService;

@Configuration
public class WebServiceEndpointConfig extends WebServiceEndpointSupport {

	@Autowired
	private BannerService bannerService;
	
	@Autowired
	private HeadService headService;
	
	@Bean
	public Endpoint bannerServiceEndpoint() {
		return publishEndpoint(bannerService, "bannerService");
	}
	
	@Bean
	public Endpoint headServiceEndpoint() {
		return publishEndpoint(headService, "headService");
	}
}
