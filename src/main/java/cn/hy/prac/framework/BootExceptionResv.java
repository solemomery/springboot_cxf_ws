package cn.hy.prac.framework;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.HandlerExceptionResolver;
import org.springframework.web.servlet.ModelAndView;


/**
 * 统一异常处理
 * @author Administrator
 *
 */
@Configuration
public class BootExceptionResv implements HandlerExceptionResolver {

	@Override
	public ModelAndView resolveException(HttpServletRequest request, HttpServletResponse response, Object object,
			Exception exception) {
		// TODO Auto-generated method stub
		return null;
	}

	@Bean
	// 初始化异常处理
	public HandlerExceptionResolver initExceptionResolver() {
		return new BootExceptionResv();
	}
}
