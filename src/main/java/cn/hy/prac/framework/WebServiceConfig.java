package cn.hy.prac.framework;

import org.apache.cxf.transport.servlet.CXFServlet;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;

@Configuration
@ImportResource({ "classpath:META-INF/cxf/cxf.xml" })
public class WebServiceConfig {

	@Bean(name = "cxfServlet")
	public ServletRegistrationBean cxfServletRegistrationBean() {
		CXFServlet servlet = new CXFServlet();
		ServletRegistrationBean bean = new ServletRegistrationBean(servlet,
				"/bus/*");
		bean.setLoadOnStartup(1);
		return bean;
	}

}
