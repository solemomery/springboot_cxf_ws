package cn.hy.prac.framework;

import javax.sql.DataSource;

import org.mybatis.spring.SqlSessionFactoryBean;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.core.io.ClassPathResource;

@Configuration
@MapperScan("cn.hy.prac.dao")
public class SqlSessionConfig {

	@Autowired
	@Qualifier("dsgltc")
	private DataSource dsgltc;

	@Bean(name = "dbhy")
	@Primary
	public SqlSessionFactoryBean getDbhy() {
		SqlSessionFactoryBean dbhy = new SqlSessionFactoryBean();
		dbhy.setDataSource(dsgltc);
		// 开启自动驼峰命名规则映射
		dbhy.setConfigLocation(new ClassPathResource("mybatis.xml"));
		return dbhy;
	}
}
