package cn.hy.prac.framework;

import org.springframework.beans.BeanUtils;
import org.springframework.context.ApplicationContext;

import cn.hy.prac.constant.Constant;
import cn.hy.prac.entities.banner.Banner;

public class SessionUtil {

private static ApplicationContext applicationContext;
	
	public static void applicationContext(ApplicationContext applicationContext) {
		SessionUtil.applicationContext = applicationContext;
	}
	
	public static Banner getUserSessEntity(){
		return (Banner)applicationContext.getBean(Constant.SESSION_USER_KEY);
	}
	
	public static void clearUserSessEntity(){
		Banner userSessEntity =  (Banner)applicationContext.getBean(Constant.SESSION_USER_KEY);
		
		BeanUtils.copyProperties(new Banner(), userSessEntity);
	}
}
