package cn.hy.prac.framework;

import javax.xml.ws.Endpoint;

import org.apache.cxf.Bus;
import org.apache.cxf.jaxws.EndpointImpl;
import org.springframework.beans.factory.annotation.Autowired;

public abstract class WebServiceEndpointSupport {
	
	@Autowired
	private Bus bus;

	protected final Endpoint publishEndpoint(Object serviceImpl, String address) {
		Endpoint endpoint = new EndpointImpl(bus, serviceImpl);
		endpoint.publish(address);
		return endpoint;
	}
}
