package cn.hy.prac.entities.head;

public class Head {
    private Integer id;

    private String sfzmhm;

    private String jxmc;

    private String zwtzm;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getSfzmhm() {
        return sfzmhm;
    }

    public void setSfzmhm(String sfzmhm) {
        this.sfzmhm = sfzmhm == null ? null : sfzmhm.trim();
    }

    public String getJxmc() {
        return jxmc;
    }

    public void setJxmc(String jxmc) {
        this.jxmc = jxmc == null ? null : jxmc.trim();
    }

    public String getZwtzm() {
        return zwtzm;
    }

    public void setZwtzm(String zwtzm) {
        this.zwtzm = zwtzm == null ? null : zwtzm.trim();
    }
}