package cn.hy.prac.entities.banner;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import cn.hy.prac.constant.Constant;

@Component(Constant.SESSION_USER_KEY) 
@Scope(value="session")
public class Banner {

	private int id;
	
	private String trueName;

	private String loginName;
	
	private String sex;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTrueName() {
		return trueName;
	}

	public void setTrueName(String trueName) {
		this.trueName = trueName;
	}

	public String getLoginName() {
		return loginName;
	}

	public void setLoginName(String loginName) {
		this.loginName = loginName;
	}

	public String getSex() {
		return sex;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}
	
	
}
