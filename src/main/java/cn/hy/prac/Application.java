package cn.hy.prac;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import cn.hy.prac.framework.WebConfig;



@SpringBootApplication
public class Application{

	public static void main(String[] args) {
		SpringApplication.run(WebConfig.class,args);
	}

}
